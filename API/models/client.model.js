const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Client
let Client = new Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  providers: [
    {
      type: Schema.Types.ObjectId,
      ref: "Provider"
    }
  ]
},{
    timestamps: true
});

module.exports = mongoose.model('Client', Client);
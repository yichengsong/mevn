const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Provider
let Provider = new Schema({
    name: {
      type: String
    },
  },
  {
    timestamps: true
  }
)

module.exports = mongoose.model('Provider', Provider);
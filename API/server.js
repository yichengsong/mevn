const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 4000;
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./DB.js');
const routes = require('./routes');
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

mongoose.Promise = global.Promise;
mongoose.connect(config.DB, { 
  useNewUrlParser: true, 
}).then(
  () => { console.log('Database is connected') },
  err => { console.log('Can not connect to the database'+ err)}
);

const corsOptions ={
  origin:'http://localhost:8080', 
  credentials:true,            //access-control-allow-credentials:true
  optionSuccessStatus:200
}


app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/', routes);

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "REST APIs",
      description:
        "handle client and provider data",
    },
  },
  apis: [__dirname + "/core/*"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

try {
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
} catch (err) {
  console.error("unable to read swagger.json", err);
}
app.listen(PORT, function(){
  console.log('Server is running on Port:',PORT);
});
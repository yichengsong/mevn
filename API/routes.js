const express = require('express');
const routes = express.Router();

// Require Provider model in our routes module
let Provider = require('./core/provider');
let Client = require('./core/client')

// Defined get all client data route
routes.route('/client/all').get(Client.getAll);

// Defined get client data by id route
routes.route('/client/:id').get(Client.getById);

// Defined create route
routes.route('/client/create').post(Client.createClient);

//  Defined update route
routes.route('/client/update/:id').put(Client.updateClient);

// Defined delete | remove | destroy route
routes.route('/client/delete/:id').delete(Client.deleteClient);

// Defined get all provider data route
routes.route('/provider/all').get(Provider.getAll);

// Defined get provider data by id route
routes.route('/provider/:id').get(Provider.getById);

// Defined create route
routes.route('/provider/create').post(Provider.createProvider);

// Defined update route
routes.route('/provider/update/:id').put(Provider.updateProvider);

// Defined delete | remove | destroy route
routes.route('/provider/delete/:id').delete(Provider.deleteProvider);


module.exports = routes;
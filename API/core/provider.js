
  // Require Client model in our routes module
let Client = require('../models/client.model');

// Require Provider model in our routes module
let Provider = require('../models/provider.model');
  
class ProviderController {
    /**
     * @swagger
     * /provider/{id}:
     *   get:
     *      parameters:
     *       - name: id
     *         in: path
     *         schema:
     *            type: string
     *         required: true
     *      responses:
     *         200:
     *            description: Returns a provider.
     */
    static getById = async (req, res) => {
        let id = req.params.id;
        Provider.findById(id, function(err, provider){
            if(err){
            res.json(err);
            }
            else {
            res.json(provider);
            }
        });
    };

    /**
   * @swagger
   * /provider/all:
   *   get:
   *     responses:
   *       200:
   *         description: Returns providers.
   */

    static getAll = (req, res) => {
        Provider.find(function(err, providers){
          if(err){
            res.json(err);
          }
          else {
            res.json(providers);
          }
        });
      }

    /**
   * @swagger
   * /provider/create:
   *   post:
   *      parameters:
   *       - name: body
   *         in: body
   *         required: true
   *         schema:
   *           type: object
   *      responses:
   *         200:
   *            description: Create a provider.
   */

    static createProvider = (req, res) => {
        const data = req.body;
        
        Provider.create(data, function (err, provider){
            if(err) {
              res.json(err);
            }
            res.json(provider);
        });
      }

     /**
   * @swagger
   * /provider/update/{id}:
   *   put:
   *      parameters:
   *       - name: id
   *         in: path
   *         schema:
   *            type: string
   *         required: true
   *       - name: body
   *         in: body
   *         required: true
   *         schema:
   *           type: object
   *      responses:
   *         200:
   *            description: Update a provider.
   */

    static updateProvider = (req, res) => {
        Provider.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, provider) {
          if(err) {
            res.json(err);
          }
          res.json(provider);
        });
    }

    /**
   * @openapi
   * /provider/delete/{id}:
   *   delete:
   *      parameters:
   *       - name: id
   *         in: path
   *         schema:
   *            type: string
   *         required: true
   *      responses:
   *         200:
   *            description: Delete a provider.
   */

    static deleteProvider = async (req, res) => {
        try {
          const provider = await Provider.findByIdAndRemove({_id: req.params.id}, {});
          await Client.updateMany({}, {$pull: {providers: provider._id}})
          res.json(provider);
        } catch(err) {
          res.status(500).send(err)
        }
    }
}

module.exports = ProviderController

import Vue from 'vue'
import VueRouter from 'vue-router'
import Client from '../views/Client.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Client',
    component: Client
  },
]

const router = new VueRouter({
  routes
})

export default router

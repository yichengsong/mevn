# mevnproject

## Current Node Version installed
18.0.0
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

## Run API
```
cd API/
```

Pull Mongo Image from Docker:

```
docker-compose up -d
```

install Node Packages
```
yarn install
```

Run server

```
yarn start
```

import axios from 'axios'

const apiHost = 'http://localhost:4000'


export const getProviders = async() => {
    const data = await axios.get(`${apiHost}/provider/all`);
    return data.data
}

export const createProvider = async(newProvider) => {
    const data = await axios.post(`${apiHost}/provider/create`, newProvider);
    return data.data
}

export const updateProvider = async(providerId) => {
    const data = await axios.put(`${apiHost}/provider/update/${providerId}`);
    return data.data
}

export const deleteProvider = async(providerId) => {
    const data = await axios.delete(`${apiHost}/provider/delete/${providerId}`)
    return data.data
}

export const getClients = async() => {
    const data = await axios.get(`${apiHost}/client/all`);
    return data.data
}

export const createClient = async(newClient) => {
    const data = await axios.post(`${apiHost}/client/create`, newClient)
    return data.data
}

export const updateClient = async(clientId, updatedClient) => {
    const data = await axios.put(`${apiHost}/client/update/${clientId}`, updatedClient);
    return data.data
}

export const deleteClient = async(clientId) => {
    const data = await axios.delete(`${apiHost}/client/delete/${clientId}`)
    return data.data
}